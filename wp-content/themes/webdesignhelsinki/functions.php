<?php
/**
 * Danny's Skeleton child theme
 * Containing nothing other than 
 * the bare bone files 
 */

// Add your functions below



/* Disable the Admin Bar. */
add_filter( 'show_admin_bar', '__return_false' );


// load Codekit CSS after style.css
/*
function high_priority_style() {
	printf( "<link rel='stylesheet' id='important-css'  href='%s' type='text/css' media='all' />\n", get_stylesheet_directory_uri() . '/codekit/codekit.css' );
}
add_action('wp_head', 'high_priority_style', 8);
*/






/*// bigger priority loads later. default = 10
add_action('the_post', 'postFunction', 10);
    function postFunction() { 
      echo "<h1>pdf link here</h1>";
}

*/

//http://wordpress.org/support/topic/error-404-on-pagination-when-changing-posts_per_page-on-query_posts#post-1553417
function remove_page_from_query_string($query_string)
{
if ($query_string['name'] == 'page' && isset($query_string['page'])) {
unset($query_string['name']);
// 'page' in the query_string looks like '/2', so i'm spliting it out
list($delim, $page_index) = split('/', $query_string['page']);
$query_string['paged'] = $page_index;
}
return $query_string;
}
// I will kill you if you remove this. I died two days for this line
add_filter('request', 'remove_page_from_query_string');





/*function theme_slug_filter_the_title( $title ) {
    $custom_title = 'YOUR CONTENT GOES HERE';
    $title .= $custom_title;
    return $title;
}

if ( is_main_query() {
  # code...
}
add_filter( 'the_title', 'theme_slug_filter_the_title' );

*/

//Add scripts to head
add_action('wp_footer', 'footerScripts', 10);
function footerScripts() { ?>

<!--analytics-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54763488-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
  
//analytics events
jQuery('.wpcf7-submit').on('click', function() {
  ga('send', 'event', 'jquery--contactform-button', 'click', 'jQuery-tracking');

});

jQuery('.btn-important').on('click', function() {
  ga('send', 'event', 'jquery--package-click', 'click', 'jQuery-tracking');

});


</script>



<!--lazyload-->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.lazyload.min.js"></script>
<script>
	jQuery("img.lazy").lazyload();
</script>
<?php }








/*------------------------------------*\
    WPML FUNCTIONS
\*------------------------------------*/

/*
//WPML shortcodes
function wpml_translate( $atts, $content = null ) {
    extract(shortcode_atts(array('lang'      => '',), $atts));
    $lang_active = ICL_LANGUAGE_CODE;   
    if($lang == $lang_active){
        return $content;
    }
}

add_shortcode("wpml_translate", "wpml_translate");
*/

/*
// WPML dont load useless stuff
define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
define('ICL_DONT_LOAD_LANGUAGES_JS', true);*/


// add shortened language codes to the navbar
function new_nav_menu_items309676($items,$args) {
    if (function_exists('icl_get_languages') && $args->theme_location == 'navi_nav') { // 'navi_nav', 'mobile_nav', 'main_nav', 'simple_nav', 'socialinks_nav',
        $languages = icl_get_languages('skip_missing=0');
        if(1 < count($languages)){
            foreach($languages as $l){
                if( $l['active'] != 1 ) { 
                    $items = $items.'<li class="menu-item-'.$l['language_code'].'"><a href="'. $l['url'].'">'. strtoupper( $l['language_code'] ).'</a></li>'; // to capitalize, replace 'strtopupper' with 'ucfirst' 

                }
            }
        }
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'new_nav_menu_items309676',10,2 );

// add shortened language codes to the mobile nav
function new_nav_menu_items3096762($items,$args) {
    if (function_exists('icl_get_languages') && $args->theme_location == 'mobile_nav') { // 'navi_nav', 'mobile_nav', 'main_nav', 'simple_nav', 'socialinks_nav',
        $languages = icl_get_languages('skip_missing=0');
        if(1 < count($languages)){
            foreach($languages as $l){
                if( $l['active'] != 1 ) { 
                    $items = $items.'<li class="menu-item-'.$l['language_code'].'"><a href="'. $l['url'].'">'. strtoupper( $l['language_code'] ).'</a></li>';
                }
            }
        }
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'new_nav_menu_items3096762',10,2 );










/*
add_action('wp_head', 'loadDetectizr');
    function loadDetectizr() { ?>
    <!--kokko-->
 <script src=" <?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js"></script>
 <script src=" <?php echo get_stylesheet_directory_uri(); ?>/js/detectizr.js"></script> 
<script>
	Detectizr.detect({detectScreen:false});
</script>

<!-- BACKSTRECT-->
<script>
//if not a crappy windows phone
if( !jQuery( "html" ).hasClass( "windowsphone" ) ){

// load backstrech.js
url = "http://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js";
jQuery.getScript(url)
	.done(function() { // backstrech loaded successfully, so initialize it:
		var backstretchImg = "/wp-content/themes/dms-child/img/iisa-crop-medium2.jpg";
  	//jQuery(document).ready(function() {
  		  jQuery("body").backstretch([backstretchImg]);
  	//});

  	// Refresh if windowHeight changes (iPhone safari scroll url-bar thinghy)
  	var lastWindowHeight = window.innerHeight; //define height of the window
  	(function() {        
	    var timer;
	    jQuery(window).bind('scroll',function () {
	        clearTimeout(timer);
	        timer = setTimeout( refresh , 150 );
	    });

	    var refresh = function () { // when user stops scrolling
	        if (lastWindowHeight != window.innerHeight) { //if the height of window has changed (iOS safari address-bars appear/disappear)
	    		jQuery.backstretch([backstretchImg]); //then we'll call backstrech to make the bg right for new window size
	    		lastWindowHeight = window.innerHeight; //and update the last known window height so we can compare it again to the possibly new winwod height
	    		}
	    };
		})();
	})
	.fail(function() {
		// boo, fall back to something else 
		//alert (" backstretch loading failed");
});
}
</script>
<!-- BACKSTRECT END-->



    <?php
}

add_action('pagelines_page', 'detectizrAndBackstretch', 0);
    function detectizrAndBackstretch() { ?>

    	*/