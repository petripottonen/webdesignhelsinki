<?php
/*
	Section: PDF
	Author: PageLines
	Author URI: http://www.pagelines.com
	Description: Adds a highlight sections with a splash image and 2-big lines of text.
	Class Name: pdfloop
	Workswith: templates, main, header, morefoot, sidebar1, sidebar2, sidebar_wrap
	Cloning: true
	Filter: component
	Loading: active
*/

/**
 * Highlight Section
 *
 * @package PageLines DMS
 * @author PageLines
 */
class pdfloop extends PageLinesSection {

	var $tabID = 'highlight_meta';


	function section_opts(){
		$opts = array(
			array(
				'type' 			=> 'select',
				'title' 		=> __( 'Select Format', 'pagelines' ),
				'key'			=> '_highlight_splash_position',
				'label' 		=> __( 'Highlight Format', 'pagelines' ),
				'opts'=> array(
					'top'			=> array( 'name' => __( 'H1', 'pagelines' ) ),
					'bottom'	 	=> array( 'name' => __( 'H2' , 'pagelines' ))
					
				),
			),
			'hl_text' => array(
				'type' 			=> 'multi',
				'col'			=> 2,
				'title' 		=> __( 'Highlight Text', 'pagelines' ),
				'opts'	=> array(
					array(
						'key'			=> 'category_name',
						'version' 		=> 'pro',
						'type' 			=> 'text',
						'size'			=> 'big',
						'label' 		=> __( 'Kategorian Nimi (polkutunnus, ei isoja kirjaimia tai välilyöntejä)', 'pagelines' ),
					),
					array(
						'key'			=> 'number_of_posts',
						'version' 		=> 'pro',
						'type' 			=> 'text',
						'size'			=> 'big',
						'label' 		=> __( 'Montako artikkelia näytetään?', 'pagelines' ),
					),

				)
			),
	

		);

		return $opts;

	}

	/**
	*
	* @TODO document
	*
	*/
	function section_template() {

		$category_name = $this->opt('category_name', $this->tset);
		$number_of_posts = $this->opt('number_of_posts', $this->tset);

		$h_subhead = $this->opt('_highlight_subhead', $this->tset);

		$h_splash = $this->opt('_highlight_splash', $this->tset);
		$h_splash_position = $this->opt('_highlight_splash_position');

		$frame_class = ($this->opt('_highlight_image_frame')) ? 'pl-imageframe' : '';

		

		?>
		<div class="highlight-area pdfloop">
			

<?php $custom_query = new WP_Query('category_name=' . $category_name . '&posts_per_page=' . $number_of_posts); // exclude category 9
while($custom_query->have_posts()) : $custom_query->the_post(); ?>

	<li <?php post_class("pdf-li"); ?> id="post-<?php the_ID(); ?>">
	<!--	<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>-->
		<?php 
		$oravaAttachment = types_render_field("orava-attachment", array("raw"=>"true","separator"=>";")); 
		//$liittenNimi = types_render_field("liitteen-nimi", array("raw"=>"true","separator"=>";")); 

		?>
		
		<a class="pdflink test" target="_blank" href="<?php echo types_render_field("orava-attachment", array("raw"=>"true","separator"=>";")); ?>"><i class="fa fa-file-pdf-o"></i> <?php  the_title(); ?> (.pdf)</a>
		

		 
		<?php //  the_content(); ?>
	</li>

<?php endwhile; ?>
<?php wp_reset_postdata(); // reset the query ?>
<br>
<a class="smallcapslink" href="/<?php echo $category_name; ?>">Vanhemmat julkaisut <i class=" fa fa-angle-double-right  "></i></a>



		</div>
	<?php

	}
}