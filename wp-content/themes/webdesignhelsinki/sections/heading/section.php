<?php
/*
	Section: Heading
	Author: PageLines
	Author URI: http://www.pagelines.com
	Description: Adds a highlight sections with a splash image and 2-big lines of text.
	Class Name: KetriHeading
	Workswith: templates, main, header, morefoot, sidebar1, sidebar2, sidebar_wrap
	Cloning: true
	Filter: component
	Loading: active
*/

/**
 * Highlight Section
 *
 * @package PageLines DMS
 * @author PageLines
 */
class KetriHeading extends PageLinesSection {

	var $tabID = 'highlight_meta';


	function section_opts(){
		$opts = array(

			'hl_text' => array(
				'type' 			=> 'multi',
				'col'			=> 2,
				'title' 		=> __( 'Highlight Text', 'pagelines' ),
				'opts'	=> array(
					array(
						'key'			=> '_highlight_head',
						'version' 		=> 'pro',
						'type' 			=> 'text',
						'size'			=> 'big',
						'label' 		=> __( 'Highlight Header Text (Optional)', 'pagelines' ),
					)

				)
			),
			array(
						'type' 			=> 'select',
						'key'			=> 'textbox_title_wrap',
						'label' 		=> __( 'Title wrapper', 'pagelines' ),
						'default'		=> 'strong',
						'opts'			=> array(
							'strong'		=> array('name' => '&lt;strong&gt; (default)'),
							'h1'			=> array('name' => '&lt;h1&gt;'),
							'h2'			=> array('name' => '&lt;h2&gt;'),
							'h3'			=> array('name' => '&lt;h3&gt;'),
							'h4'			=> array('name' => '&lt;h4&gt;'),
							'h5'			=> array('name' => '&lt;h5&gt;'),
							'none'			=> array('name' => 'none'),
						)
					)
	

		);

		return $opts;

	}

	/**
	*
	* @TODO document
	*
	*/
	function section_template() {
		$title_wrap = ( '' != $this->opt( 'textbox_title_wrap' ) ) ? $this->opt( 'textbox_title_wrap' ) : 'strong';
		$h_head = $this->opt('_highlight_head', $this->tset);
	
		// create an ID from the content of the heading
		$h_id = $h_head;
		$h_id = str_replace('ä', 'a', $h_id); // Replaces all ä with a. 
		$h_id = str_replace('ö', 'o', $h_id); // Replaces all ö with o. 
		$h_id = str_replace(' ', '-', $h_id); // Replaces all spaces with hyphens. 
		$h_id = preg_replace('/[^A-Za-z0-9\-]/', '', $h_id); // Removes special chars. 
		$h_id = strtolower($h_id); // Convert to lowercase   

		//$h_id = clean($h_id);
		//$h_id = preg_replace("/[^A-Za-z0-9]/", "", $h_id);
		

		$h_subhead = $this->opt('_highlight_subhead', $this->tset);

		$h_splash = $this->opt('_highlight_splash', $this->tset);
		$h_splash_position = $this->opt('_highlight_splash_position');

		$frame_class = ($this->opt('_highlight_image_frame')) ? 'pl-imageframe' : '';

		if(!$h_head && !$h_subhead && !$h_splash){
			$h_head = __("Here's to the crazy ones...", 'pagelines');
			
		}

		?>
		<div class="highlight-area ketriheading">
			<?php



switch ($title_wrap) {
  case "h1":
    printf('<h1 id="%s" class="ketri-heading" data-sync="_highlight_head">%s</h1>', __( $h_id,  'pagelines' ),  __( $h_head,  'pagelines' ) );
    break;
  case "h2":
   printf('<h2 id="%s" class="ketri-heading" data-sync="_highlight_head">%s</h2>', __( $h_id,  'pagelines' ),  __( $h_head,  'pagelines' ) );
    break;
  case "h3":
   printf('<h3 id="%s" class="ketri-heading" data-sync="_highlight_head">%s</h3>', __( $h_id,  'pagelines' ),  __( $h_head,  'pagelines' ) );
    break;
  case "h4":
  printf('<h4 id="%s" class="ketri-heading" data-sync="_highlight_head">%s</h4>', __( $h_id,  'pagelines' ),  __( $h_head,  'pagelines' ) );
  	break;
  case "h5":
  printf('<h5 id="%s" class="ketri-heading" data-sync="_highlight_head">%s</h5>', __( $h_id,  'pagelines' ),  __( $h_head,  'pagelines' ) );
  	break;
  case "h6":
  	printf('<h6 id="%s" class="ketri-heading" data-sync="_highlight_head">%s</h6>', __( $h_id,  'pagelines' ),  __( $h_head,  'pagelines' ) );
   	break;
  default:
    	printf('<strong id="%s" class="ketri-heading" data-sync="_highlight_head">%s</strong>', __( $h_id,  'pagelines' ),  __( $h_head,  'pagelines' ) );
}




			
			?>
		</div>
	<?php

	}
}