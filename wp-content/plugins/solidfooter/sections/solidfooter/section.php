<?php
/*
	Section: SolidFooter
	Author: PageLines
	Author URI: http://www.pagelines.com
	Description: SolidFooter is a free DMS Section based on the popular solid footer used on www.pagelines.com
	Class Name: PLSolidFooter
	Loading: active
	Version: 1.0
	PageLines: true
	v3: true
*/


class PLSolidFooter extends PageLinesSection {

	function section_opts(){
		$options = array(
			array(
				'type'	=> 'multi',
				'key'	=> 'sl_urls', 
				'title'	=> 'Link URLs',				
				'opts'	=> pl_social_links_options()
			),
			array(
				'type'	=> 'multi',
				'col'	=> 2,
				'title'	=> 'Information',
				'opts'	=> array(
					array(
						'key'	=> 'title',
						'type'	=> 'text',
						'label'	=> __( 'Title', 'pagelines' ),
					),
					array(
						'key'		=> 'desc',
						'label'		=> __( 'Text description', 'pagelines' ),
						'type'		=> 'textarea'
					),
					array(
						'key'	=> 'terms',
						'type'	=> 'text',
						'label'	=> __( 'Terms', 'pagelines' ),
					),
					array(
						'key'		=> 'logo',
						'label'		=> __( 'Big Logo', 'pagelines' ),
						'type'		=> 'image_upload',
						'has_alt'		=> true
					),
					array(
						'key'	=> 'menu',
						'type'	=> 'select_menu',
						'label'	=> __( 'Select Menu', 'pagelines' ),
					),
					array(
						'key'		=> 'disable_search',
						'label'		=> __( 'Disable Search', 'pagelines' ),
						'type'		=> 'check',
						'default'	=> false
					),
					array(
						'key'		=> 'disable_logo',
						'label'		=> __( 'Disable Logo', 'pagelines' ),
						'type'		=> 'check',
						'default'	=> false
					),
					array(
						'key'		=> 'disable_shares',
						'label'		=> __( 'Disable Shares', 'pagelines' ),
						'type'		=> 'check',
						'default'	=> false
					),
					array(
						'key'		=> 'disable_terms',
						'label'		=> __( 'Disable Terms', 'pagelines' ),
						'type'		=> 'check',
						'default'	=> false
					)
				)
			)
			
		);

		
		

		return $options;
	}
	
	function section_persistent() {
		register_nav_menus( array( 'solidfooter_nav' => __( 'SolidFooter Section', 'pagelines' ) ) );
	}


	function section_template( ) {
		
		$desc_default = 'PageLines has a passion for web design and development. We sell <a href="http://www.pagelines.com/product-category/themes/">premium drag &amp; drop themes</a> and <a href="http://www.pagelines.com/shop/">plugins</a> for WordPress. In addition, we help others learn web design, and we also have a strong community that builds and sells things on the <a href="">PageLines Store</a>.';
		$terms_default = '&copy; 2014 PageLines. All Rights Reserved. | <a href="http://www.pagelines.com/product-category/themes/">Premium WordPress Themes</a>';
		$menu = $this->opt('menu', array( 'default' => false ) );		
		$terms = $this->opt( 'terms', array( 'default' => $terms_default ) );
		$logo = $this->image( 'logo', $this->base_url .'/footer-logo.png' );
		$title = $this->opt( 'title', array( 'default' => sprintf( 'About %s', get_bloginfo( 'description' ) ) ) );		
		$desc = $this->opt( 'desc', array( 'default' => $desc_default ) );
		$disable_search = $this->opt( 'disable_search', array( 'default' => false ) );
		$disable_logo = $this->opt( 'disable_logo', array( 'default' => false ) );
		$disable_shares = $this->opt( 'disable_shares', array( 'default' => false ) );
		$disable_terms = $this->opt( 'disable_terms', array( 'default' => false ) );
		
	 ?>
		<div class="solidfooter-wrap">
			<div class="pl-content">
				<div class="pl-content-pad fix">
					<div class="solidfooter-main fix">
						<div class="solidfooter-left">							
							<?php
							if( $menu || has_nav_menu( 'solidfooter_nav' ) ): ?>
								<div class="space">
								<?php
									$menu_args = array(
										'theme_location' 	=> 'solidfooter_nav',
										'menu' 				=> $menu,
										'menu_class'		=> 'solidfooter-nav',
										'no_toggle'			=> true
									);
									echo pl_navigation( $menu_args );
								?>
								</div>
							<?php endif; ?>
							<?php if( true != $disable_search ): ?>
							<div class="space">
							<?php 	
								pagelines_search_form( true, 'solidfooter-searchform searchform');
							?>
							</div>
							<?php endif; ?>
							<div class="space">
								<h3><?php echo $title;?></h3>
								<p><?php echo $desc;?></p>
							</div>
							<div class="space">
								<?php echo pl_social_links(); ?>
							</div>
						</div>
						<?php if( true != $disable_logo ): ?>
						<div class="solidfooter-right">
							<?php printf('<div class="solidfooter-logo"><a href="%s">%s</a></div>', home_url('/'), $logo); ?>
						</div>
						<?php endif; ?>
					</div>
					<div class="solidfooter-terms pl-border">
						<?php if( true != $disable_shares ): ?>
						<div class="solidfooter-left">
							<?php 
								$facebook = ( pl_setting('facebook_name') ) ? pl_setting('facebook_name') : 'pagelines';
								echo do_shortcode( sprintf( '[like_button url="http://www.facebook.com/%s"]', $facebook ));

								echo do_shortcode('[googleplus]');

								echo do_shortcode('[twitter_button type="follow"]');
							?>
						</div>
						<?php endif; ?>
						<?php if( true != $disable_terms ): ?>
						<div class="solidfooter-right ">
							<div class="terms-text"><?php echo $terms;?></div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
<?php }


}