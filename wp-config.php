<?php

// live site url for detection
$remoteSiteUrl = "webdesignhelsinki.fi";

//local or remote variables
if(strpos($_SERVER['HTTP_HOST'], $remoteSiteUrl) !== false) {$environment = "remote";} else { $environment = "local";}

//modify your copy of WordPress to work with any domain name  -http://clickontyler.com/support/a/21/wordpress-liftoff/
$s = empty($_SERVER['HTTPS']) ? '' : ($_SERVER['HTTPS'] == 'on') ? 's' : '';
$port = ($_SERVER['SERVER_PORT'] == '80') ? '' : (":".$_SERVER['SERVER_PORT']);
// koska ei oo /wp/ ni kommentoitu tää versio ja käytetää seuraavaa
//$url = "http$s://" . $_SERVER['HTTP_HOST'] ."/oravastaging" . $port;
$url = "http$s://" . $_SERVER['HTTP_HOST'] . $port;
//$url = "http$s://" . $_SERVER['HTTP_HOST'] ."/wp" . $port;
//$url = "http$s://" . $_SERVER['HTTP_HOST'] . $port;
define('WP_HOME', $url);
define('WP_SITEURL', $url);
unset($s, $port, $url);


/** 
 * WordPressin perusasetukset.
 *
 * Tämä tiedosto sisältää seuraavat asetukset: MySQL-asetukset, Tietokantataulun etuliite,
 * henkilökohtaiset salausavaimet (Secret Keys), WordPressin kieli, ja ABSPATH. Löydät lisätietoja
 * Codex-sivulta {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php}. Saat MySQL-asetukset palveluntarjoajaltasi.
 *
 * Automaattinen wp-config.php-tiedoston luontityökalu käyttää tätä tiedostoa
 * asennuksen yhteydessä. Sinun ei tarvitse käyttää web-asennusta, vaan voit 
 * tallentaa tämän tiedoston nimellä "wp-config.php" ja muokata allaolevia arvoja.
 *
 * @package WordPress
 */


/* 
 * Unified variables 
 */   
//$hostname = 'localhost';  
$chartset = 'utf8';  
$collate = 'utf8_swedish_ci';  
$pl_hooks_php = 'true';  

/* 
 * Check for the current environment 
 */  

if($environment === "remote" ){
  $db_name = 'ketri_webdesignhelsinki';  
  $user_name = 'ketri_ketri'; 
  $password = 'usrJrzDqkJG6';  
  $hostname = 'localhost';  
}
else{
  $db_name = 'webdesignhelsinki';  
  $user_name = 'root'; 
  $password = 'root';  
  $hostname = 'localhost';  
}

  

// ** MySQL settings - You can get this info from your web host ** //  
/** The name of the database for WordPress */  
define('DB_NAME', $db_name);  
  
/** MySQL database username */  
define('DB_USER', $user_name);  
  
/** MySQL database password */  
define('DB_PASSWORD', $password);  
  
/** MySQL hostname */  
define('DB_HOST', $hostname);  
  
/** Database Charset to use in creating database tables. */  
define('DB_CHARSET', $chartset);  
  
/** The Database Collate type. Don't change this if in doubt. */  
define('DB_COLLATE', $collate);  



/** Pagelines enable PHP in hooks */  
define( 'PL_HOOKS_PHP', $pl_hooks_php);






/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Muuta nämä omiksi uniikeiksi lauseiksi!
 * Voit luoda nämä käyttämällä {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org palvelua}
 * Voit muuttaa nämä koska tahansa. Kaikki käyttäjät joutuvat silloin kirjautumaan uudestaan.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~;`CSYAO:q;,49Bj5:E#+?6o>J_;&;%$c3?C,7|xCD8KBquVxwcQAo+l_Gg}_*^z');
define('SECURE_AUTH_KEY',  '1 x2,P4[XMcgGkJt5}<|([ $M{`6gM!+j303&uw|`1+S7vf+Z+8m:.NXcbRHz;Dq');
define('LOGGED_IN_KEY',    'zd$&dVnt8CVK;M4#%Y134G/Zp|UUJl;5-g7IOQ(K-Ew~t<S/+<T/QX%v.Y;mwqbf');
define('NONCE_KEY',        'ddHYXfCOM9#u[gT,fy}F0t*zI(eKqo9>sON]yj@tF=@ULgK#e,[%XVRFk4%iD:Ab');
define('AUTH_SALT',        'w]X}uA{om(8pEG_Z-?RF.bcUtroShebttxVec(8R?o|}G@fxJ%Q:8ip^1O>k|oYo');
define('SECURE_AUTH_SALT', 'jFHkT-i~icboM=`5HeSCVN}+6eWp&eV2=n)T ~xZgNla?s#$Z$`j!XB&qhl>E59u');
define('LOGGED_IN_SALT',   '>QbF&jB-:}x3E(rUuK0%&l<>9<Z*^N9-AAoh,jy|FHe_tg$|]B3m _XP:Zj+rh,B');
define('NONCE_SALT',       '|v-prgEN_)6g6KKxyK.;]c_j5cMzlEG@0Qz=Bb#(NNP?tx<!#Y<2Dn(jO/q.QL;5');


/**#@-*/

/**
 * WordPressin tietokantataulujen etuliite (Table Prefix).
 *
 * Samassa tietokannassa voi olla useampi WordPress-asennus, jos annat jokaiselle
 * eri tietokantataulujen etuliitteen. Sallittuja merkkejä ovat numerot, kirjaimet
 * ja alaviiva _.
 *
 */
$table_prefix  = 'wp_Vz7uL_';

/**
 * WordPressin kieli.
 *
 * Muuta tämä WordPressin kieliasetusten muuttamiseksi. Vastaavasti nimetty 
 * kielitiedosto pitää asentaa hakemistoon wp-content/languages. Esimerkiksi,
 * asenna de.mo wp-content/languages -hakemistoon ja muuta WPLANG:in arvoksi 'de'
 * käyttääksesi WordPressiä saksan kielellä.
 */
define ('WPLANG', 'fi');

/**
 * Kehittäjille: WordPressin debug-moodi.
 *
 * Muuta tämän arvoksi true jos haluat nähdä kehityksen ajan debug-ilmoitukset
 * Tämä on erittäin suositeltavaa lisäosien ja teemojen kehittäjille.
 */
define('WP_DEBUG', false);

/* Siinä kaikki, älä jatka pidemmälle! */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
